/*
    Essa é uma implementação da árvore de busca para o ponto mais próximo do ponto informado utilizando uma árvore binária
    Esse algoritmo é chamado de KdTree
*/

/**
 * Array com todos os pontos
 */
const data = [ [4,7], [7,13], [9,4], [11,10], [14,11], [16, 10], [15, 3] ];

/**
 * Ponto para ser pesquisado
 * Caso queira pesquisar outro ponto é só mudar as coordenadas dessa variável
 */
const pivot = [14, 9];

getClosest(pivot, data);


function buildKdTree(points, depth = 0){
    let n = points.length;
    if(n <= 0) return;

    const axis = depth % 2;
    const sortedPoints = points.sort((a,b)=>a[axis] - b[axis]);
    const median = Math.floor(n /2);

    return {
        value: sortedPoints[median],
        left: buildKdTree(sortedPoints.slice(0, median), depth +1),
        right: buildKdTree(sortedPoints.slice(median + 1), depth +1)
    }
}

function closestKdTree(root, point, depth = 0){
    if(!root) return null;
    const axis = depth % 2;

    let next_branch = null;
    let opposite_branch = null;
    if(point[axis] < root.value[axis]){
        next_branch = root.left;
        opposite_branch = root.right;
    }else{
        next_branch = root.right;
        opposite_branch = root.left;
    }

    let best = closer_distance(point, closestKdTree(next_branch, point, depth +1), root.value);

    if(distance_squared(point, best) > (point[axis] - root.value[axis])**2){
        best = closer_distance(point, closestKdTree(opposite_branch, point, depth +1), best);
    }

    return best;
}

function closer_distance(pivot, p1, p2){
    if(!p1) return p2;
    if(!p2) return p1;

    d1 = distance_squared(pivot, p1);
    d2 = distance_squared(pivot, p2);

    return d1 < d2 ? p1 : p2;
}

function distance_squared(point1, point2){
    let x1,x2;
    let y1,y2;
    if(!point1.location){
        [x1, y1] = point1;
    }else{
        [x1, y1] = point1.location;
    }
    
    if(!point2.location){
        [x2, y2] = point2;
    }else{
        [x2, y2] = point2.location;
    }
    const dx = x1 - x2;
    const dy = y1 - y2;

    return dx * dx + dy * dy
}

function getClosest(pivot, allPoints){
    const kdtree = buildKdTree(allPoints);
    const found = closestKdTree(kdtree, pivot);
    const expected = closestPoint(allPoints, pivot);

    const expectedDistance = Math.sqrt(distance_squared(pivot, expected));
    const foundDistance = Math.sqrt(distance_squared(pivot, found));

    console.log(`Esperado: ${expected} (Distância: ${expectedDistance})`);
    console.log(`Encontrado: ${found} (Distância: ${foundDistance})`);

    if(foundDistance > expectedDistance){
        console.error('Foi encontrado uma distância maior do que esperado!');
    }
}

function closestPoint(allPoints, newPoint){
    let bestPoint = null;
    let bestDistance = null;
    for(let point in allPoints){
        let currentDistance = distance_squared(newPoint, allPoints[point]);
        if(bestDistance == null || currentDistance < bestDistance){
            bestDistance = currentDistance;
            bestPoint = allPoints[point];
        }
    }

    return bestPoint;
}